import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { User } from './user.model';
import { Subject } from 'rxjs';
import { API_URL } from '../app.constants';

export const AUTHENTICATED_USER = 'authenticatedUser';
export const TOKEN = 'token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInUser = new Subject<string>();

  constructor(
    private http: HttpClient
  ) { }

  authenticateUser(username: string, password: string) {
    return this.http.post<any>(`${API_URL}/authenticate`, { username, password }).pipe(
      map(
        data => {
          sessionStorage.setItem(AUTHENTICATED_USER, username);
          sessionStorage.setItem(TOKEN, `Bearer ${data.jwt}`);
          this.loggedInUser.next(AUTHENTICATED_USER);
          return data;
        }
      )
    )

  }

  registerUser(user: User) {
    return this.http.post<User>(`${API_URL}/users/register`, user);

  }

  getAuthenticatedUser() {
    return sessionStorage.getItem(AUTHENTICATED_USER);
  }

  getToken() {
    return sessionStorage.getItem(TOKEN);
  }

  isUserLoggedIn() {
    if (sessionStorage.getItem(AUTHENTICATED_USER)) {
      return true;
    }
    else {
      return false;
    }

  }

  logOut() {
    sessionStorage.removeItem(AUTHENTICATED_USER);
    sessionStorage.removeItem(TOKEN);
    this.loggedInUser.next(null);

  }


}
