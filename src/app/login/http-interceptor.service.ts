import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private authSvc: AuthService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler){

    let authHeaderString = this.authSvc.getToken();
    let username = this.authSvc.getAuthenticatedUser();

    if(authHeaderString && username){
      req = req.clone({
        setHeaders: {
          Authorization : authHeaderString
        }
      })
    }

    return next.handle(req);

  }
}
