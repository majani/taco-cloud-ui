export class User {
    public id?:string;
    public username: string;
    public password: string;
    public fullname: string;
    public street: string;
    public city: string;
    public state: string;
    public zip: string;
    public phoneNumber: string;

    constructor(username: string, password: string, fullname: string, street: string, city: string, state: string, zip: string, phoneNumber:string) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.phoneNumber = phoneNumber;
    }
}