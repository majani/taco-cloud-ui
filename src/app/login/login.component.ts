import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from './user.model';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoginMode = true;

  constructor(
    private authSvc: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  registerUser(form: NgForm) {
    let newUser = new User(form.value.username, form.value.password, form.value.fullname, form.value.street, form.value.city, form.value.state, form.value.zipCode, form.value.phoneNumber);
    this.authSvc.registerUser(newUser).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/login']);
      },

      error => {
        console.log(form);
        console.log(error);
      }
    )

  }

  logIn(form: NgForm) {

    this.authSvc.authenticateUser(form.value.username, form.value.password).subscribe(
      data => {
        //console.log(data);
        this.router.navigate(['/home']);
      },
      error => {
        console.log(error);
      }
    )


    console.log(form);
  }

}
