import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../app.constants';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  model = {
    name: '',
    street: '',
    state: '',
    zip: '',
    city: '',
    ccNumber: '',
    ccExpiration: '',
    ccCVV: '',
    tacos: []
  };

  constructor(
    private cart: CartService,
    private httpClient: HttpClient
  ) { }

  ngOnInit() {
  }

  get cartItems() {
    return this.cart.getItemsInCart();
  }

  get cartTotal() {
    return this.cart.getCartTotal();
  }

  onSubmit() {
    this.cart.getItemsInCart().forEach(
      cartItem => {
        this.model.tacos.push(cartItem.taco);
      }
    );
    console.log(this.model);

    this.httpClient.post(`${API_URL}/orders`, this.model).subscribe(
      r => this.cart.emptyCart()
    );
  }

}
