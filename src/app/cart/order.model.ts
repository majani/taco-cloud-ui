import { Taco } from '../design/taco.model';

export class Order {
    public name:string;
    public street:string;
    public state:string;
    public zip:string;
    public ccNumber:string;
    public ccExpiration:string;
    public ccCVV:string;
    public tacos: Taco [];

    constructor(name:string, street:string, state:string, zip:string, ccNumber:string, ccExpiration:string,ccCVV:string,tacos: Taco []){
        this.name = name;
        this.street = street;
        this.state = state;
        this.zip = zip;
        this.ccNumber = ccNumber;
        this.ccExpiration = ccExpiration;
        this.ccCVV = ccCVV;
        this.tacos = tacos;

    }
}