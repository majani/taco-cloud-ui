import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CartService } from '../cart/cart.service';
import { Taco } from '../design/taco.model';
import { Router } from '@angular/router';
import { API_URL } from '../app.constants';

@Component({
  selector: 'app-recents',
  templateUrl: './recents.component.html',
  styleUrls: ['./recents.component.css']
})
export class RecentsComponent implements OnInit {
  recentTacos: Taco[];
  p:number = 1;
  constructor(
    private httpClient: HttpClient,
    private cart: CartService,
    private router: Router
  ) { }

  ngOnInit() {
    this.httpClient.get<Taco[]>(`${API_URL}/design/recent`).subscribe(
      data => {
        this.recentTacos = data;
      }
    )
  }

  orderTaco(taco:Taco){
    this.cart.addToCart(taco);
    this.router.navigate(['/cart']);

  }

}
