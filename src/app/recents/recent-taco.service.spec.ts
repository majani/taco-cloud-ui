import { TestBed } from '@angular/core/testing';

import { RecentTacoService } from './recent-taco.service';

describe('RecentTacoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecentTacoService = TestBed.get(RecentTacoService);
    expect(service).toBeTruthy();
  });
});
