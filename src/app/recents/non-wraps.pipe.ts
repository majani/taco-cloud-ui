import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nonWraps'
})
export class NonWrapsPipe implements PipeTransform {

  transform(ingredients: any, ...args: any[]): any {
    const nonWraps = [];

    for (const ingredient of ingredients) {
      if (ingredient.type !== 'WRAP') {
        nonWraps.push(ingredient);
      }
    }

    return nonWraps;
  }

}
