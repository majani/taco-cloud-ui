import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CartService } from '../cart/cart.service';
import { Ingredient } from './ingredients.model';
import { API_URL } from '../app.constants';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.css']
})
export class DesignComponent implements OnInit {

  model = {
    name: '',
    ingredients: []
  };
  allIngredients:any;
  wraps = [];
  proteins = [];
  veggies = [];
  cheeses = [];
  sauces = [];

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private cart: CartService
  ) { }

  ngOnInit() {
    this.httpClient.get<Ingredient[]>(`${API_URL}/ingredients`).subscribe(
      data => {
        this.allIngredients = data;
        this.wraps = this.allIngredients.filter(w => w.type === 'WRAP');
        this.proteins = this.allIngredients.filter(p => p.type === 'PROTEIN');
        this.veggies = this.allIngredients.filter(v => v.type === 'VEGGIES');
        this.cheeses = this.allIngredients.filter(c => c.type === 'CHEESE'),
        this.sauces = this.allIngredients.filter(s => s.type === 'SAUCE');
      }
    );
  }

  updateIngredients(ingredient, event){
    if(event.target.checked){
      this.model.ingredients.push(ingredient);
    }
    else {
      this.model.ingredients.splice(this.model.ingredients.findIndex(i => i === ingredient), 1);
    }
  }

  onSubmit(){
    this.httpClient.post(`${API_URL}/design`, this.model).subscribe(
      taco => this.cart.addToCart(taco)
    );
    this.router.navigate(['/cart']);
  }

}
