import { Ingredient } from './ingredients.model';

export class Taco {
    public name:string;
    public ingredients: Ingredient[];

    constructor(name: string, ingredients: Ingredient[]){
        this.name = name;
        this.ingredients = ingredients;
    }
}