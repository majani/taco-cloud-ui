import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RecentsComponent } from './recents/recents.component';
import { SpecialComponent } from './special/special.component';
import { LocationsComponent } from './locations/locations.component';
import { DesignComponent } from './design/design.component';
import { CartComponent } from './cart/cart.component';
import { LogoutComponent } from './logout/logout.component';
import { RouteguardService } from './login/routeguard.service';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [RouteguardService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'recents',
    component: RecentsComponent,
    canActivate: [RouteguardService]
  },
  {
    path: 'specials',
    component: SpecialComponent,
    canActivate: [RouteguardService]
  },
  {
    path: 'locations',
    component: LocationsComponent,
    canActivate: [RouteguardService]
  },
  {
    path: 'design',
    component: DesignComponent,
    canActivate: [RouteguardService]
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [RouteguardService]
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [RouteguardService]
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
