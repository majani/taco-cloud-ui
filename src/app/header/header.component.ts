import { Component, OnInit, OnDestroy } from '@angular/core';
import { CartService } from '../cart/cart.service';
import { AuthService } from '../login/auth.service';
import { Subscription } from 'rxjs';
import { User } from '../login/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  loggedIn: Subscription;
  user: string;
  cart: CartService;
  constructor(
    cart: CartService,
    private authSvc: AuthService
  ) {
    this.cart = cart;
  }

  ngOnInit() {
    this.loggedIn = this.authSvc.loggedInUser.subscribe(
      data => {
        console.log(data);
        this.user = data;
      }
    )
    
  }

  ngOnDestroy(){
    this.loggedIn.unsubscribe();
  }

}
