import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatButtonToggleModule, MatCheckboxModule, MatRadioModule } from '@angular/material';

const MaterialComponents = [
  MatButtonModule,
  MatCardModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatRadioModule
]


@NgModule({

  imports: [
    MaterialComponents
  ],

  exports: [
    MaterialComponents
  ]
})
export class MaterialModule { }
